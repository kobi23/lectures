#include <vector>
#include "phone.h"
#include "xiaomi.h"
#include "nokia.h"
#include "iphone.h"

Phone* GetPhone(int parameters)
{
    if (parameters == dynamic_cast<Xiaomi*>(new Xiaomi())->get_properties())
        return new Xiaomi();

    if (parameters == dynamic_cast<Nokia*>(new Nokia())->get_properties())
        return new Nokia();

    if (parameters == dynamic_cast<IPhone*>(new IPhone())->get_properties())
        return new IPhone();

    return nullptr;
}
