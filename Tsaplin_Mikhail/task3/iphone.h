#ifndef TASK1_IPHONE_H
#define TASK1_IPHONE_H

class IPhone : public Phone
{
public:
	IPhone();
	~IPhone();
    void UsePhone() override;
    void Call() override;
	void TakePhoto() override;
    void Pay() override {};
};


#endif