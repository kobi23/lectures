#ifndef TASK1_XIAOMI_H
#define TASK1_XIAOMI_H

class Xiaomi : public Phone
{
public:
	Xiaomi();
	~Xiaomi();
    void UsePhone() override;
    void Call() override;
    void TakePhoto() override {};
    void Pay() override;
};


#endif