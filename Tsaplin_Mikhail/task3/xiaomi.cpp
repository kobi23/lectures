#include <iostream>
#include "phone.h"
#include "xiaomi.h"

using namespace std;

Xiaomi::Xiaomi()
{
	this->set_properties(2);
	this->set_name("Xiaomi");
	this->set_price(15000);
}

void Xiaomi::UsePhone()
{
	this->Call();
	this->Pay();
}

void Xiaomi::Call()
{
	cout << "Some calling from user" << endl;
}

void Xiaomi::Pay()
{
	cout << "Some paying by user" << endl;
}

Xiaomi::~Xiaomi(){}
