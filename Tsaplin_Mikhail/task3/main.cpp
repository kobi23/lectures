#include <vector>
#include <list>
#include <map>
#include <set>
#include <iterator>
#include <random>
#include <cstdlib>
#include <iostream>
#include "freeman.h"
#include "granny.h"
#include "instagirl.h"
#include "interface.h"
#include "xiaomi.h"
#include "nokia.h"
#include "iphone.h"

using namespace std;

int main()
{
    auto granny = new Granny();
    auto freeman = new Freeman();
    auto instagirl = new InstaGirl();

	granny->device = GetPhone(granny->get_properties());
	freeman->device = GetPhone(freeman->get_properties());
	instagirl->device = GetPhone(instagirl->get_properties());
	
	granny->LaunchProcess();
	freeman->LaunchProcess();
	instagirl->LaunchProcess();

	delete granny;
	delete freeman;
	delete instagirl;

	vector<Phone*> phones = {new Xiaomi(), new Nokia(), new IPhone()};
    for(auto it = phones.begin(); it != phones.end(); it++)
    {
        it.operator*()->SetNumber(rand());
    }
    for(auto it = phones.begin(); it != phones.end(); it++)
    {
        cout << it.operator*()->GetNumber() << " " <<  it.operator*()->get_name() << endl;
        it.operator*()->Call();
        it.operator*()->GetInfo();
    }

    list<Phone*> phones_list = {new Xiaomi(), new Nokia(), new IPhone()};
    for(auto it = phones_list.begin(); it != phones_list.end(); it++)
    {
        it.operator*()->SetNumber(rand());
    }
    for(auto it = phones_list.begin(); it != phones_list.end(); it++)
    {
        cout << it.operator*()->GetNumber() << " " <<  it.operator*()->get_name() << endl;
        it.operator*()->TakePhoto();
    }

    set<string> strings;

    strings.insert("fihgeighiu");
    strings.insert("HOHOHO");
    strings.insert("HelloWorld");

    for(const auto & string : strings)
    {
        cout << string << endl;
    }

    strings.erase("HOHOHO");

    for(const auto & string : strings)
    {
        cout << string << endl;
    }

    map<uint32_t, Phone*> phones_map;

    phones_map[800553535] = new Xiaomi();
    phones_map[4333331] = new Nokia();
    phones_map[312557] = new IPhone();

    for(auto it = phones_map.begin(); it != phones_map.end(); it++)
    {
        cout << it.operator*().first << " " <<  it.operator*().second->get_name() << endl;

    }

    return 0;
}
