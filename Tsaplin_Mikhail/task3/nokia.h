#ifndef TASK1_NOKIA_H
#define TASK1_NOKIA_H

class Nokia : public Phone
{
public:
	Nokia();
	~Nokia();
    void UsePhone() override;
	void Call() override;
    void TakePhoto() override {};
    void Pay() override {};
};


#endif