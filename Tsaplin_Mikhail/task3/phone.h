#ifndef TASK1_PHONE_H
#define TASK1_PHONE_H

#include <string>
#include <iostream>

using namespace std;

class Phone
{
public:
    virtual void UsePhone() = 0;

    int get_properties() { return this->properties; }
    void set_properties(int value) {this->properties = value;}

    string get_name() { return this->name; }
    void set_name(string value) { this->name = value; }

    int get_price() { return this->price; }
    void set_price(int value) { this->price = value; }

    uint32_t GetNumber() { return this->number; }
    void SetNumber(uint32_t value) { this->number = value; }

    void GetInfo() { cout << this->get_name() << " " << this->get_price() << endl; }

    virtual void Call() = 0;
    virtual void TakePhoto() = 0;
    virtual void Pay() = 0;

private:
    int properties;
    string name;
    int price;
    uint32_t number;
};


#endif //TASK1_PHONE_H
