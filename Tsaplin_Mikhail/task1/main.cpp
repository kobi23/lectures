#include "user.h"
#include "freeman.h"
#include "granny.h"
#include "instagirl.h"
#include "interface.h"

using namespace std;

int main()
{
    auto granny = new Granny();
    auto freeman = new Freeman();
    auto instagirl = new InstaGirl();

	granny->device = GetPhone(Granny::properties);
	freeman->device = GetPhone(Freeman::properties);
	instagirl->device = GetPhone(InstaGirl::properties);
	
	granny->LaunchProcess();
	freeman->LaunchProcess();
	instagirl->LaunchProcess();
	
	return 0;
}
