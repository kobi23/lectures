#ifndef TASK1_PHONE_H
#define TASK1_PHONE_H

class Phone
{
public:
    virtual void UsePhone() = 0;
    inline static int properties;
};


#endif //TASK1_PHONE_H
