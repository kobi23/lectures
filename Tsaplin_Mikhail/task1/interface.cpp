#include <vector>
#include "phone.h"
#include "xiaomi.h"
#include "nokia.h"
#include "iphone.h"

Phone* GetPhone(int parameters)
{
    if (parameters == Xiaomi::properties)
        return new Xiaomi();

    if (parameters == Nokia::properties)
        return new Nokia();

    if (parameters == IPhone::properties)
        return new IPhone();

    return nullptr;
}
