#include <iostream>
#include "phone.h"
#include "xiaomi.h"

using namespace std;

Xiaomi::Xiaomi()
{
	Xiaomi::properties = 2;
}

void Xiaomi::UsePhone()
{
	this->call();
	this->pay();
}

void Xiaomi::call()
{
	cout << "Some calling from user" << endl;
}

void Xiaomi::pay()
{
	cout << "Some paying by user" << endl;
}

Xiaomi::~Xiaomi(){}
