#ifndef TASK1_GRANNY_H
#define TASK1_GRANNY_H

#include "user.h"

class Granny : public User
{
public:
	Granny();
	virtual ~Granny();
	void LaunchProcess() override;
};

#endif