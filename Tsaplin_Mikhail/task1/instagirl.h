#ifndef TASK1_INSTAGIRL_H
#define TASK1_INSTAGIRL_H

#include "user.h"

class InstaGirl : public User
{
public:
	InstaGirl();
	virtual ~InstaGirl();
	void LaunchProcess() override;
};

#endif