#include <iostream>
#include <vector>
#include "phone.h"
#include "iphone.h"

using namespace std;

IPhone::IPhone()
{
	IPhone::properties = 1;
}

void IPhone::UsePhone()
{
	this->call();
	this->take_photo();
}

void IPhone::call()
{
	cout << "Some calling from user" << endl;
}

void IPhone::take_photo()
{
	cout << "Taking some photo by user" << endl;
}

IPhone::~IPhone(){}
