#ifndef TASK1_USER_H
#define TASK1_USER_H

#include "phone.h"

using namespace std;

class User 
{
public:
	virtual void LaunchProcess() = 0;

    inline static int properties;
	Phone* device = nullptr;
};



#endif //TASK1_USER_H
