cmake_minimum_required(VERSION 3.6.1)
project(task1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

set(SOURCE_FILES freeman.cpp granny.cpp instagirl.cpp interface.cpp iphone.cpp nokia.cpp xiaomi.cpp main.cpp phone.h user.h)

add_executable(test ${SOURCE_FILES})
