#ifndef TASK1_FREEMAN_H
#define TASK1_FREEMAN_H

#include "user.h"

class Freeman : public User
{
public:
	Freeman();
	virtual ~Freeman();
	void LaunchProcess() override;
};

#endif