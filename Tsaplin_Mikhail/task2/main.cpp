#include "freeman.h"
#include "granny.h"
#include "instagirl.h"
#include "interface.h"


int main()
{
    auto granny = new Granny();
    auto freeman = new Freeman();
    auto instagirl = new InstaGirl();

	granny->device = GetPhone(granny->get_properties());
	freeman->device = GetPhone(freeman->get_properties());
	instagirl->device = GetPhone(instagirl->get_properties());
	
	granny->LaunchProcess();
	freeman->LaunchProcess();
	instagirl->LaunchProcess();
	
	return 0;
}
