#ifndef TASK1_PHONE_H
#define TASK1_PHONE_H

#include <string>

using namespace std;

class Phone
{
public:
    virtual void UsePhone() = 0;

    int get_properties() { return this->properties; }
    void set_properties(int value) {this->properties = value;}

    string get_name() { return this->name; }
    void set_name(string value) { this->name = value; }

    int get_price() { return this->price; }
    void set_price(int value) { this->price = value; }

    virtual void call() = 0;

private:
    int properties;
    string name;
    int price;
};


#endif //TASK1_PHONE_H
