#include <iostream>
#include "phone.h"
#include "xiaomi.h"

using namespace std;

Xiaomi::Xiaomi()
{
	this->set_properties(2);
	this->set_name("Xiaomi");
	this->set_price(15000);
}

void Xiaomi::UsePhone()
{
	this->call();
	this->pay();
}

void Xiaomi::call()
{
	cout << "Some calling from user" << endl;
}

void Xiaomi::pay()
{
	cout << "Some paying by user" << endl;
}

Xiaomi::~Xiaomi(){}
