#ifndef TASK1_USER_H
#define TASK1_USER_H

#include "phone.h"

class User
{
public:
	virtual void LaunchProcess() = 0;

	int get_properties() { return this->properties; }
	void set_properties(int value) { this->properties = value; }

    Phone* device = nullptr;

private:
    int properties;
};



#endif //TASK1_USER_H
