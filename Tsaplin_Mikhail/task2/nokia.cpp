#include <iostream>
#include "phone.h"
#include "nokia.h"

using namespace std;

Nokia::Nokia()
{
	this->set_properties(0);
	this->set_name("Nokia");
	this->set_price(2000);
}

void Nokia::UsePhone()
{
	this->call();
}

void Nokia::call()
{
	cout << "Some calling from user" << endl;
}

Nokia::~Nokia(){}