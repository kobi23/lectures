#include <iostream>
#include <vector>
#include "phone.h"
#include "iphone.h"

using namespace std;

IPhone::IPhone()
{
	this->set_properties(1);
	this->set_name("IPhone");
	this->set_price(150000);
}

void IPhone::UsePhone()
{
	this->call();
	this->take_photo();
}

void IPhone::call()
{
	cout << "Some calling from user" << endl;
}

void IPhone::take_photo()
{
	cout << "Taking some photo by user" << endl;
}

IPhone::~IPhone(){}
