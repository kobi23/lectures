#include <iostream>
#include <basetyps.h>

using namespace std;

interface ITurn_on_phone{
    virtual void turn_on_phone() = 0;
};

interface ICall{
    virtual void call() = 0;
};

interface IWeb{
    virtual void internet() = 0;

};

interface IPhoto{
    virtual void take_photo() = 0;
};

interface IPay{
    virtual void pay() = 0;
};

interface IPrice{
    virtual void price() = 0;
};

interface IName{
    virtual void name() = 0;
};

class IPhone: ICall{
public:
    void call() override {
        cout << "Call" << endl;
    }

};

class User: ITurn_on_phone{
public:
    void turn_on_phone()override {
        cout << "User turn on phone" << endl;
    }

};

class Xiaomi: IPhone, IPay, IPrice, IName{
public:
    void name()override {
        cout << "Name phone: Xiaomi mi8" << endl;
    }
    void price()override {
        cout << "Price phone Xiaomi 300$" << endl;
    }
    void call() override{
        cout << "Call" << endl;
    }
    void pay() override {
        cout << "Pay" << endl;
    }
};

class Apple: IPhone, IWeb, IPhoto, IPrice, IName{
public:
    void name()override {
        cout << "Name phone: Iphone 11" << endl;
    }
    void price()override {
        cout << "Price phone Apple 1000$" << endl;
    }
    void call() override{
        cout << "Call" << endl;
    }
    void internet() override {
        cout << "Browses the Internet" << endl;
    }
    void take_photo() override {
        cout << "Take photo" << endl;
    }

};

class Honor: IPhone,IPhoto, IPrice, IName{
public:
    void name()override {
        cout << "Name phone: Honor 20" << endl;
    }
    void call() override{
        cout << "Call" << endl;
    }
    void price()override {
        cout << "Price phone Honor 500$" << endl;
    }
    void take_photo() override {
        cout << "Take photo" << endl;
    }
};

class User_Xiaomi: User, Xiaomi{
public:
    Xiaomi xiaomi;
    void turn_on_phone()override {
        cout << "User turn on phone" << endl;
        xiaomi.name();
        xiaomi.call();
        xiaomi.pay();
        xiaomi.price();
    }
};

class User_Apple: User, Apple{
public:
    Apple apple;
    void turn_on_phone()override {
        cout << "User turn on phone" << endl;
        apple.name();
        apple.call();
        apple.take_photo();
        apple.internet();
    }
};

class User_Honor: User, Honor{
public:
    Honor honor;
    void turn_on_phone()override {
        cout << "User turn on phone" << endl;
        honor.name();
        honor.price();
        honor.call();
        honor.take_photo();
    }

};
int main() {
User_Xiaomi Andrey;
Andrey.turn_on_phone();
Andrey.xiaomi.name();
Andrey.xiaomi.price();
Andrey.xiaomi.pay();
Andrey.xiaomi.call();

User_Apple Tom;
Tom.turn_on_phone();
Tom.apple.name();
Tom.apple.price();
Tom.apple.call();
Tom.apple.internet();
Tom.apple.take_photo();

User_Honor Bob;
Bob.turn_on_phone();
Bob.honor.name();
Bob.honor.price();
Bob.honor.call();
Bob.honor.take_photo();
}