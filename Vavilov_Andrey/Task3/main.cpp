#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>

using namespace std;

std::vector<std::string> v_name_phone = {"Xiaomi mi8","Iphone 11" ,"Honor 20"};
std::vector<int> v_number = {578769, 576789, 574321};
auto iter = v_number.cbegin();
auto iter2 = ++v_number.cbegin();
auto iter3 = iter +2;

std::list<std::string> l_photo = {"Photo_forest","Photo_man" ,"Photo_girl"};
auto iter_list_photo = l_photo.cbegin();
auto iter_list_photo1 = ++l_photo.cbegin();


map<string, int > directory = {{"Xiaomi mi8",578769},
                               {"Iphone 11",576789},
                               {"Honor 20",543221}};
auto iter_map = directory.cbegin();
auto iter_map1 = ++directory.cbegin();


class ITurn_on_phone{
        virtual void Turn_on_phone() = 0;
};

class ICall{
        virtual void Call() = 0;
};

class IWeb{
        virtual void Internet() = 0;

};

class IPhoto{
        virtual void Take_photo() = 0;
};

class IPay{
        virtual void Pay() = 0;
};

class Iinfo{
    virtual void Price() = 0;
    virtual void Name() = 0;
    virtual void Directory() = 0;
};


class IPhone: ICall{
public:
    void Call() override {
        cout << "Call" << endl;
    }
};

class User: ITurn_on_phone{
public:
    void Turn_on_phone()override {
        cout << "User turn on phone" << endl;
    }
};

class Xiaomi: IPhone, IPay, Iinfo{

public:
    void Name()override {
        cout << "Name phone: " << v_name_phone[0] << '\n';
//            cout << "Name phone: Xiaomi mi8" << endl;
    }

    void Price()override {
            cout << "Price phone Xiaomi 300$" << endl;
    }

    void Call() override{
        while (iter != v_number.cend()){
            cout << "Call: " << *iter << '\n';
            iter++;
        }
//        cout << "Call" << endl;
    }
    void Pay() override {
        cout << "Pay" << endl;
    }
    void Directory()override {
        cout << "Directory of numbers Xiaomi mi8: " << endl;
        for (iter_map; iter_map != directory.cend(); iter_map++){
            cout << iter_map->first << " : " << iter_map->second << endl;
        }
    }
};

class Apple: IPhone, IWeb, IPhoto, Iinfo{
public:
    void Name()override {
        cout << "Name phone: " << v_name_phone[1] << '\n';
//        cout << "Name phone: Iphone 11" << endl;
    }
    void Price()override {
        cout << "Price phone Apple 1000$" << endl;
    }

    void Call() override{
        for (iter2; iter2!=v_number.end(); ++iter2){
            cout << "Call: " << *iter2 << '\n';
        }
//        cout << "Call" << endl;
    }
    void Internet() override {
        cout << "Browses the Internet" << endl;
    }
    void Take_photo() override {
        for(iter_list_photo; iter_list_photo != l_photo.cend(); iter_list_photo++){
            cout << "Take photo: " << *iter_list_photo << '\n';
        }
    }
    void Directory()override {
        cout << "Directory of numbers Iphone 11: " << endl;
        cout << "Number Xiaomi mi8: " << directory["Xiaomi mi8"] <<", Number Honor 20: " << directory["Honor 20"];
    }
};

class Honor: IPhone,IPhoto, Iinfo{
public:
    void Name()override {
        cout << "Name phone: " << v_name_phone[2] << '\n';
    }
    void Call() override{
        for (iter3; iter3!=v_number.end(); ++iter3){
            cout << "Call: " << *iter3 << '\n';
        }
    }
    void Price()override {
        cout << "Price phone Honor 500$" << endl;
    }
    void Take_photo() override {
        for (iter_list_photo1; iter_list_photo1 != l_photo.cend(); iter_list_photo1++){
            cout << "Take photo: " << *iter_list_photo1 << '\n';
        }
    }
    void Directory()override {
        cout << "Directory of numbers Honor 20: " << endl;
        for (iter_map1; iter_map1 != directory.cend(); iter_map1++){
            cout << iter_map1->first << " : " << iter_map1->second << endl;
        }
    }
};

class User_Xiaomi: User, Xiaomi{
public:
    Xiaomi xiaomi;
    void Turn_on_phone()override {
        cout << "User_Xiaomi turn on phone" << endl;
        xiaomi.Name();
        xiaomi.Price();
        xiaomi.Call();
        xiaomi.Pay();
        xiaomi.Directory();
    }
};

class User_Apple: User, Apple{
public:
    Apple apple;
    void Turn_on_phone()override {
        cout << "User_Apple turn on phone" << endl;
        apple.Name();
        apple.Price();
        apple.Call();
        apple.Take_photo();
        apple.Internet();
        apple.Directory();
    }
};

class User_Honor: User, Honor{
public:
    Honor honor;
    void Turn_on_phone()override {
        cout << "User_Honor turn on phone" << endl;
        honor.Name();
        honor.Price();
        honor.Call();
        honor.Take_photo();
        honor.Directory();
    }

};

int main() {
    User_Xiaomi Andrey;
    Andrey.Turn_on_phone();

    User_Apple Tom;
    Tom.Turn_on_phone();

    User_Honor Bob;
    Bob.Turn_on_phone();

    std::list<std::string> l_name_phone = {"Xiaomi mi8","Iphone 11" ,"Honor 20"};
    std::list<int> l_number = {578904, 575533, 543221};
    auto iter_list = l_name_phone.cbegin();
    auto iter_list1 = l_number.cbegin();

    for(iter_list; iter_list != l_name_phone.cend(); iter_list++){
        cout << "Print list phone: " << *iter_list << '\n';
    }

    for (iter_list1; iter_list1 != l_number.cend(); iter_list1++){
        cout << "Print list number: " << *iter_list1 << '\n';
    }

    std::set<std::string> myset;
    myset.insert("Xiaomi");
    myset.insert("Apple");
    myset.insert("Honor");
    auto  it = myset.begin();
    for (it; it!=myset.end(); ++it){
        cout << "Phone manufacturers: " << *it << '\n';
    }
    auto del = myset.begin();
    for (del; del!=myset.end(); ++del){
        if(*del == "Honor"){
            myset.erase(del);
            cout << "Delete manufacturers: " << *del << '\n';
        }
    }
}