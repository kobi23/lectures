#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>

int main()
{
    class IPhone // abstract phone
    {
    public: virtual void get_price() = 0;
          virtual void get_name() = 0;
          void call() {
              std::cout << "Ring-ring\n";
          }
          virtual ~IPhone() {};
    };

    class IPhoto
    {
    public: virtual void photo() = 0;
          virtual ~IPhoto() {};
    };

    class IInternet
    {
    public: virtual void internet() = 0;
          virtual void pay() = 0;
          virtual ~IInternet() {};
    };

    class IUser // abstract user
    {
    public: virtual void use_phone() = 0;
          virtual ~IUser() {};
    };

    class Telephone : public IPhone // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Telephone\n";
        }
    };

    class Photophone : public IPhone, IPhoto // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1500\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Photophone\n";
        }
        void photo() override { // Function override
            std::cout << "Click\n";
        }
    };

    class Netphone : public IPhone, IInternet // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "3000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Netphone\n";
        }
        void internet() override { // Function override
            std::cout << "Website\n";
        }
        void pay() override { // Function override
            std::cout << "Scam\n";
        }
    };

    class Normal : public IUser // User inheritance
    {
    public:
        Telephone phone;
        void use_phone() override { // Function override
            phone.call();
        }
    };

    class Photographer : public IUser // User inheritance
    {
    public:
        Photophone phone;
        void use_phone() override { // Function override
            phone.photo();
        }

    };

    class Victim : public IUser // User inheritance
    {
    public:
        Netphone phone;
        void use_phone() override { // Function override
            phone.internet();
            phone.pay();
        }

    };

    // A test

    /*Normal normal;
    Photographer photographer;
    Victim victim;

    std::cout << "Normal\n";
    normal.use_phone();
    std::cout << "\nPhotographer\n";
    photographer.use_phone();
    std::cout << "\nVictim\n";
    victim.use_phone();*/

    class Phones
    {
        Telephone telephone;
        Photophone photophone;
        Netphone netphone;

    public:
        void get_phone(int i) {
            switch (i) {
            case 0:
                get_telephone();
                break;
            case 1:
                get_photophone();
                break;
            case 2:
                get_netphone();
                break;
            default:
                break;
            }
        }
        void get_telephone() {
            telephone.get_name();
            telephone.get_price();
            telephone.call();
        }
        void get_photophone() {
            photophone.get_name();
            photophone.get_price();
            photophone.call();
            photophone.photo();
        }
        void get_netphone() {
            netphone.get_name();
            netphone.get_price();
            netphone.call();
        }
    };

    int i = 0;
    Phones phoneOne;
    Phones phoneTwo;
    Phones phoneThree;

    std::cout << "Vector:\n";
    std::vector<Phones>phoneVector(3);
    phoneVector.push_back(phoneOne);
    phoneVector.push_back(phoneTwo);
    phoneVector.push_back(phoneThree);
    for (i = 0; i < 3; i++) {
        phoneVector.at(i).get_phone(i);
        std::cout << "\n";
    }

    std::cout << "\nList:\n";
    std::list<Phones>phoneList(3);
    phoneList.push_back(phoneOne);
    phoneList.push_back(phoneTwo);
    phoneList.push_back(phoneThree);
    for (i = 0; i < phoneList.size(); i++) {
        phoneList.front().get_phone(i);
        phoneList.pop_front();
        std::cout << "\n";
    }

    std::cout << "\nSet:\n";
    std::set<std::string> mySet;
    mySet.insert("test");
    mySet.erase("test");

    std::cout << "\nMap:\n";
    std::map<int,Phones> phoneMap;
    phoneMap[0] = phoneOne;
    phoneMap[1] = phoneTwo;
    phoneMap[2] = phoneThree;
    for (i = 0; i < phoneList.size(); i++) {
        phoneMap.at(i).get_phone(i);
        std::cout << "\n";
    }
}