#include <iostream>

int main()
{
    class Phone // abstract phone
    {
    public: virtual void get_price() = 0;
          virtual void get_name() = 0;
          void call() {
              std::cout << "Ring-ring\n";
          }
          virtual void photo() {
              std::cout << "This phone doesn't have this function\n";
          }
          virtual void internet() {
              std::cout << "This phone doesn't have this function\n";
          }
          virtual void pay() {
              std::cout << "This phone doesn't have this function\n";
          }
    };

    class User // abstract user
    {
    public: virtual void use_phone() = 0;
    };

    class Telephone : public Phone // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Telephone\n";
        }
    };

    class Photophone : public Phone // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1500\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Photophone\n";
        }
        void photo() override { // Function override
            std::cout << "Click\n";
        }
    };

    class Netphone : public Phone // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "3000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Netphone\n";
        }
        void internet() override { // Function override
            std::cout << "Website\n";
        }
        void pay() override { // Function override
            std::cout << "Scam\n";
        }
    };

    class Normal : public User // User inheritance
    {
    public:
        Telephone phone;
        void use_phone() override { // Function override
            phone.call();
        }
    };

    class Photographer : public User // User inheritance
    {
    public:
        Photophone phone;
        void use_phone() override { // Function override
            phone.photo();
        }

    };

    class Victim : public User // User inheritance
    {
    public:
        Netphone phone;
        void use_phone() override { // Function override
            phone.internet();
            phone.pay();
        }

    };

    // A test

    Normal normal;
    Photographer photographer;
    Victim victim;

    std::cout << "Normal\n";
    normal.use_phone();
    std::cout << "\nPhotographer\n";
    photographer.use_phone();
    std::cout << "\nVictim\n";
    victim.use_phone();
};